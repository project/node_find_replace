<?php

/**
 * @file: node_find_replace.module
 * Allows global find replace on node content
 * @author: regx <gvarnell@osoft.com>
 * @copyright: 2007 Gary Varnell
 * @license: GNU - See LICENCE.TXT
*/

/*
Description:
  The node_find_replace module allows global case-sensitive find replace on node content.
  This module keeps a history snapshot so that you can undo / redo replaces
  
  Undo/Redo only effects items in the snapshot - new nodes aren't effected
  
  Currently works on
  Node Title
  Node Body
  CCK Node Titles
  CCK Node Text
  (CCK Field titles (human readable field name) are not currently supported)

*/

//================================================================================
define('NODE_FIND_REPLACE_DEBUG', 0);

/**
 * Implementation of hook_simpletest().
 */
function node_find_replace_simpletest() {
  $dir = drupal_get_path('module', 'node_find_replace'). '/test';
  $tests = file_scan_directory($dir, '\.test');
  return array_keys($tests);
}
//---------------------------------------------------



/**
 * Implementation of hook_menu().
 */
function node_find_replace_menu($may_cache) {
  global $user;
  $items = array();

  //if ($may_cache) {

    $items[] = array('path' => 'admin/node_find_replace',
      'title' => t('find replace'),
      'access' => user_access('administer nodes'),
      'callback' => 'node_find_replace_form',
     );
  //}
  return $items;
}


/**
 * Implementation of hook_help
 */
function node_find_replace_help($section)
{
	switch($section) {
		case 'admin/modules#description':
			return t('Allows glodal find and replace on nodes.');
	}
}


function node_find_replace_form() {
  $output .= drupal_get_form('node_find_replace_form', node_find_replace_form_build());
  $output .= drupal_get_form('node_find_replace_history_form', node_find_replace_history_form_build());
  return $output;
}

function node_find_replace_form_build(){
$form = array();
  
  $form['find_replace'] = array(
    '#type' => 'fieldset',
    '#title' => t('Find Replace'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['find_replace']['find_string'] = array(
    '#type' => 'textfield',
    '#title' => t('Find'),
    '#size' => 60,
    '#required' => TRUE,
    '#maxlength' => 60,
  );
  $form['find_replace']['replace_string'] = array(
    '#type' => 'textfield',
    '#title' => t('Replace'),
    '#size' => 60,
    '#required' => TRUE,
    '#maxlength' => 60,
  );
  $form['find_replace']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Replace'),
  );
  return $form;
}

function node_find_replace_history_form_build(){
$form = array();
  
  $form['find_replace_history'] = array(
    '#type' => 'fieldset',
    '#title' => t('History'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['find_replace_history']['description'] = array(
    '#value' => t("The History is a snapshot of the replaced node revisions before and after the replace.
    Undoing a replace sets all revisions of the replaced node back to the snapshot,
    so if the node was updated after the replace, the changes will be lost unless they are in a new revision.
    <br/>After running a find replace, you should probably delete the history if everything went ok."),
   );
  $sql = "SELECT hid,count(*) as count, find_string,replace_string from node_find_replace_history GROUP BY hid";
  $result = db_query($sql);
  while($row = db_fetch_object($result)){
    $history_options[$row->hid]="hid: $row->hid  replacements:<b>$row->count</b>  find:<b>$row->find_string</b> replace:<b>$row->replace_string</b>";
  }
  
  $form['find_replace_history']['row']['hid'] = array(
    '#type' => 'radios',
    '#title' => 'History ID',
    '#required' => TRUE,
    '#options' => $history_options,
  );
    
  $form['find_replace_history']['undo'] = array(
    '#type' => 'submit',
    '#value' => t('Undo'),
  );
  $form['find_replace_history']['redo'] = array(
    '#type' => 'submit',
    '#value' => t('Redo'),
  );
  $form['find_replace_history']['delete_history'] = array(
    '#type' => 'submit',
    '#value' => t('Delete History'),
  );
  return $form;
}
function node_find_replace_history_form_submit($form_id, $form_values){
  switch($_POST['op']){
    case 'Undo':
      node_find_replace_history_undo($form_values['hid']);
      break;
    
    case 'Redo':
      node_find_replace_history_redo($form_values['hid']);
      break;
    
    case 'Delete History':
      node_find_replace_history_delete($form_values['hid']);
      break;
  }
  node_find_replace_clear_cache();
}

function node_find_replace_history_undo($hid){
  $result = db_query("SELECT * FROM node_find_replace_history where hid=%d",$hid);
  while($row = db_fetch_object($result)){
    drupal_set_message("restoring $row->nid ver $row->vid into $row->table_name",'status');
    db_query("UPDATE %s SET %s = '%s' WHERE nid=%d AND vid='%d'",$row->table_name,$row->field_name,$row->old_value,$row->nid,$row->vid);
  }
  drupal_set_message("hid:$row->hid Undo complete",'status');
}

function node_find_replace_history_redo($hid){
  $result = db_query("SELECT * FROM node_find_replace_history where hid=%d",$hid);
  while($row = db_fetch_object($result)){
    drupal_set_message("restoring $row->nid ver $row->vid into $row->table_name",'status');
    db_query("UPDATE %s SET %s = '%s' WHERE nid=%d AND vid='%d'",$row->table_name,$row->field_name,$row->new_value,$row->nid,$row->vid);
  }
  drupal_set_message("hid:$row->hid Redo Complete",'status');
}

function node_find_replace_history_delete($hid){
  db_query("DELETE FROM node_find_replace_history WHERE hid =%d",$hid);
  drupal_set_message("hid:$hid Deleted",'status');
}

function node_find_replace_form_validate($form_id, $form_values) {
  if(!$form_values['find_string']){
    form_set_error('find_string', t('Find String can not be blank.'));
  }
  if(!$form_values['replace_string']){
    form_set_error('replace_string', t('Replace String can not be blank.'));
  }
}

function node_find_replace_form_submit($form_id, $form_values) {
  //node_find_replace_debug('submit');
  $match_count = 0;
  $node_replace_count = 0;

  $find_string = $form_values['find_string'];
  $replace_string = $form_values['replace_string'];
  $replace_count = 0;

  $hid = db_next_id('node_find_replace_history_hid');
  
  # make a regx safe find string
  $rfind_string = preg_replace('/([^a-zA-Z0-9])/','\\\\$1',$find_string);
  
  find_replace_nodes($hid,$rfind_string,$replace_string);
  find_replace_cck_nodes($hid,$rfind_string,$replace_string);
  node_find_replace_clear_cache();

  drupal_goto('admin/node_find_replace');
}
//---------------------------------------------------------

/** node_find_replace_generic - Genric find replace function
  You can use this to do find replacements as long as the table you want to find replacements in
  has nid and vid fields defined. - To refactor this out requires some changes in history and the history table
  
  $hid = history id, should be same for all find replaces done in a session so this is passed in
  $table = table for find replace
  $field = field to be replaced
  $rfind string = regexp safe find string
  $replace string = replace string 
**/
function node_find_replace_generic($hid,$table,$field,$rfind_string,$replace_string){
  $revision_count = 0;
  $node_count = 0;
  
  $sql = "select * from %s where %s like '%s';"; 
  $result = db_query($sql,$table,$field,'%'.$rfind_string.'%');
  //drupal_set_message(db_num_rows($result) . "rows returned $table $field $rfind_string");
  while($row = db_fetch_array($result)){
    if(preg_match("/$rfind_string/",$row[$field]) == 1){
      $revision_count ++;
      $new_val = str_replace($rfind_string,$replace_string,$row[$field]);
      
      # save history
      $id = db_next_id('node_find_replace_history_id');
      db_query("INSERT INTO node_find_replace_history SET 
        id = %d,
        hid = %d,
        nid = %d,
        vid = %d,
        table_name = '%s',
        field_name = '%s',
        old_value = '%s',
        new_value = '%s',
        find_string = '%s',
        replace_string = '%s';"
        ,
        $id,
        $hid,
        $row['nid'],
        $row['vid'],
        $table,
        $field,
        $row[$field],
        $new_val,
        $rfind_string,
        $replace_string
      );

      # replace
      db_query("UPDATE %s set
        %s = '%s' where
        nid = %d and
        vid = %d;"
        ,
        $table,
        $field,
        $new_val,
        $row['nid'],
        $row['vid']
      );
    }
  }
  drupal_set_message("$revision_count $field field replacements made in table $table",'status');
}


function find_replace_nodes($hid,$rfind_string,$replace_string){
 node_find_replace_generic($hid,'node','title',$rfind_string,$replace_string);
 node_find_replace_generic($hid,'node_revisions','title',$rfind_string,$replace_string);
 node_find_replace_generic($hid,'node_revisions','body',$rfind_string,$replace_string);
 node_find_replace_generic($hid,'node_revisions','teaser',$rfind_string,$replace_string);
}
 
//----------------------------------------------------------

function find_replace_cck_nodes($hid,$rfind_string,$replace_string){
  if(module_exist('content')){
    # get list of cck node types
    $cck_fields = content_fields();
    node_find_replace_debug($cck_fields);
    foreach($cck_fields as $cck_field => $cck_val){
      if($cck_val['db_storage']){
        $cck_field = $cck_field . '_value';
        $cck_table = 'node_'. $cck_val['type_name'];
        #node_find_replace_debug('field: '. $cck_field);
        #node_find_replace_debug('table: '. $cck_table);
        
        node_find_replace_generic($hid,$cck_table,$cck_field,$rfind_string,$replace_string);
      }
    } # end foreach $cck_val
  }
    drupal_set_message($cck_count . ' cck fields replaced','status');
}
###########################################################################
# Helper Functions
function node_find_replace_clear_cache(){
  db_query("DELETE FROM cache");
}

function node_find_replace_debug($msg){
  if(NODE_FIND_REPLACE_DEBUG){
    if(is_array($msg) || is_object($msg)){
      drupal_set_message('<pre>' . print_r($msg,true) . '</pre>','debug');
    }
   else{
      drupal_set_message('node_find_replace: ' . $msg,'debug');
    }
  }
}